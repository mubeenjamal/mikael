## About Application

<p>In this application i have used laravel 5.8 with Passport Api Authentication</p>
<p>Version control for application is GIT.</p>
<p>Bitbucket public repository made for the application.</p>

## Installation
<p>Here is some of the steps which you have to do for setup the project</p>
<p>There is some commands for initial setup<p>

- composer install
- composer update (if required)
- php artisan migrate
- php artisan passport:install

## Configuration
- changed .env.example file to .env file
- In public folder copy .htaccess file to root path
- Also create a base_url global variable in postman environment and set the base URL of the application
