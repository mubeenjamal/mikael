<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login','Api\AuthController@login');
Route::post('signup','Api\AuthController@signUp');
Route::group(['middleware' => ['auth:api','client.credentials'],'namespace'=>'Api'], function(){
    Route::resource('posts','PostController');
    Route::post('post/comment','PostController@post_comment');
    Route::post('post/like','PostController@post_liked');
});
