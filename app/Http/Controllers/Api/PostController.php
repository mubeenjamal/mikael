<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\CommentPostRequest;
use App\Http\Requests\CreatePostRequest;
use App\Http\Requests\LikePostRequest;
use App\Http\Requests\UpdatePostRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use App\PostLikes;
use App\PostComments;
use PhpParser\Comment;
use Auth;
class PostController extends Controller
{
    public $successStatus = 200;
    public $response = array();
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::with(['comments','likes'])->paginate();
        if($posts->count()){
            $this->response = array(
                'status' => true,
                'message' => 'User Email or Password is Incorrect !',
                'data' => $posts
            );
        }else{
            $this->response = array(
                'status' => false,
                'message' => 'There is no post!'
            );
        }
        return response()->json($this->response, $this->successStatus);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePostRequest $request)
    {
        $input = $request->all();
        $input['user_id'] = Auth::user()->id;
        $post = Post::create($input);
        if($post->id){
            $this->response = array(
                'status' => true,
                'message' => 'You post has been published'
            );
        }else{
            $this->response = array(
                'status' => false,
                'message' => 'Failed to create post!'
            );
        }
        return response()->json($this->response, $this->successStatus);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePostRequest $request, $id)
    {
        $post = Post::find($id);
        $this->authorize('update',$post);
        $post->title = $request->title;
        $post->description = $request->description;
        $save = $post->save();
        if($save){
            $this->response = array(
                'status' => true,
                'message' => 'Post has been updated!'
            );
        }else{
            $this->response = array(
                'status' => false,
                'message' => 'Failed to update this post!'
            );
        }
        return response()->json($this->response, $this->successStatus);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $this->authorize('delete',$post);
        $del = Post::destroy($id);
        if($del){
            $this->response = array(
                'status' => true,
                'message' => 'Successfully Deleted this Post'
            );
        }else{
            $this->response = array(
                'status' => false,
                'message' => 'Failed to delete this post!'
            );
        }
        return response()->json($this->response, $this->successStatus);
    }

    /**
     * Like Post
     *
     * @return \Illuminate\Http\Response
     */
    public function post_liked(LikePostRequest $request){

        $input = $request->all();
        $input['user_id'] = Auth::user()->id;
        $post = Post::find($input['post_id']);
        $this->authorize('like',$post);
        $post_like = PostLikes::create($input);
        if($post_like->id){
            $this->response = array(
                'status' => true,
                'message' => 'You have liked this Post'
            );
        }else{
            $this->response = array(
                'status' => false,
                'message' => 'Failed to like this post!'
            );
        }
        return response()->json($this->response, $this->successStatus);
    }

    /**
     * Comment Post
     *
     * @return \Illuminate\Http\Response
     */
    public function post_comment(CommentPostRequest $request){

        $input = $request->all();
        $input['user_id'] = Auth::user()->id;
        $post_like = PostComments::create($input);
        if($post_like->id){
            $this->response = array(
                'status' => true,
                'message' => 'You have commented on the post'
            );
        }else{
            $this->response = array(
                'status' => false,
                'message' => 'Failed to like this post!'
            );
        }
        return response()->json($this->response, $this->successStatus);
    }

}
