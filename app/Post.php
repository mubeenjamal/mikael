<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'title', 'description',
    ];

    /**
     * Get the comments for the post.
     */
    public function comments()
    {
        return $this->hasMany('App\PostComments');
    }

    /**
     * Get the likes for the post.
     */
    public function likes()
    {
        return $this->hasMany('App\PostLikes');
    }
}
